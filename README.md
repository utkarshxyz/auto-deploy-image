# auto-deploy-image

The [Auto-DevOps](https://docs.gitlab.com/ee/topics/autodevops/) [deploy stage](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml) image.

## Contributing and Code of Conduct

Please see [CONTRIBUTING.md](CONTRIBUTING.md)
